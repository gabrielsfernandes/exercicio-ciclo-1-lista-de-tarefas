#include <iostream>
#include "lista.h"
#define BUFFER 64
int main () {
    Lista lista;
    listaInit( &lista );

    char escolha;
    int n;
    char *tarefa;
    int chave;
    do {
        //system("cls");
        fprintf(stdout, "\n Lista de Tarefas\n\n");
        fprintf(stdout, "Escolha uma opcao: \n");
        fprintf(stdout, "1 - Insere Dados\n");
        fprintf(stdout, "2 - Deleta Dados\n");
        fprintf(stdout, "3 - Busca Dados\n");
        fprintf(stdout, "4 - Exibe Dados\n");
        fprintf(stdout, "5 - Sair\n\n");

        fprintf(stdout, "Resposta: ");
        scanf("%c", &escolha);

        switch (escolha) {
            case '1':
                tarefa = (char *)malloc(BUFFER);
                fprintf(stdout, "Insira uma tarefa: ");
                scanf("%s", tarefa);

                fprintf(stdout, "Insira a posicao: ");
                scanf("%d", &n);

                listaInsere( &lista, n, tarefa);
                break;

            case '2':
                fprintf(stdout, "Informe a posicao da tarefa que deseja remover: ");
                scanf("%d", &n);
                listaRemove( &lista, n);
                fprintf(stdout, "A tarefa foi removida com sucesso");
                break;


            case '3':
                fprintf(stdout, "Insira a posicao da tarefa que deseja visualizar: ");
                scanf("%d", &chave);
                listaBuscar( &lista, chave);
               break;

            case '4':
                fprintf(stdout,"\n");
                escreveLista( &lista );
                break;
            case '5':
                exit(0);
                break;

            default:
                fprintf(stderr,"Digite uma opcao valida!\n");
                getchar();
                break;
        }

        getchar();
    }
    while (escolha > 0);
    return 0;

}